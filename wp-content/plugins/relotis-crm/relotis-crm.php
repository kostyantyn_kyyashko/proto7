<?php
/*
 *	Plugin Name: Relotis CRM
 *  Author: Relotis
 *  Description: CRM for the Relotis hosting
 *
 * License: GPLv3 or later
 * License URI: https://www.gnu.org/licenses/gpl-3.0.en.html
 * Version: 1.0.0
 * Text Domain: relotis-crm
 */
/**
 * todo sql prepare
 */
require_once 'actions.php';
require_once 'handlers.php';
require_once 'tabs_hooks.php';
require_once 'page_content.php';
require_once 'pages.php';
require_once 'relotis-customer-admin.php';



